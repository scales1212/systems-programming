#include <stdio.h>
#include <stdlib.h>

/*function declaration */
void displayArray(float *arr, int size);
void sortArray(float *arr, int size);

int main(int args, char** argv){
	int N, i;
	printf("Please enter number of elements in array: ");
	scanf("%d", &N);

	//float arr[N];
	float *arr = (float*) malloc(N * sizeof(float));
	for (i=0; i<N; i++) {
		printf("Please enter %d of array: ", (i+1));
		scanf("%f", (arr+i));
	}

	displayArray(arr, N);
	sortArray(arr, N);
	displayArray(arr, N);
}

void displayArray(float *arr, int size){
	printf("[");
	int i;
	for (i=0; i<size-1; i++) {
		printf("%f, ", *(arr+i));
	}
	printf("%f]\n", *(arr+i-1));
}

void sortArray(float *arr, int size) {
	float temp;
	int currLoc, i;
	for (i=1; i<size; i++){
		currLoc = i;
		while (currLoc > 0 && *(arr+currLoc-1) > *(arr+currLoc)) {
			temp = *(arr+currLoc);
			*(arr+currLoc) = *(arr+currLoc-1);
			*(arr+currLoc-1) = temp;
			currLoc--;
		}
	}
}


