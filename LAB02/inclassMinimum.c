#define size 100
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

int main (int args, char** argv) {
	//build random list of ints, length 100
	srand48((unsigned int)time(NULL));
	double array[size];
	int i;

	for (i=0; i<size; i++) {
		array[i] = drand48();
	}

	//find minimum in randomly generated list
	double min_value = array[0];
	int j;
	for (j=0; j<size; j++){
		if (array[j] < min_value)
			min_value = array[j];
	}

	printf("%f", min_value);
}
