#include <stdio.h>
#include <stdlib.h>

int main(int args, char** argv) {
	int given_number;
	printf("Give me a number of elements you want in the list:\n");
	scanf("%d", &given_number);
	int array[given_number];

	int i;
	int j;
	for (i=0; i<given_number;i++){
		printf("Next Number: \n");
		scanf("%d",&j);
		array[i] = j;
	}

	//insertion sort
	int temp, currLoc;
	for (i=0; i<given_number;i++){
		currLoc = i;
		while (currLoc > 0 && array[currLoc-1] > array[currLoc]){
			temp = array[currLoc];
			array[currLoc] = array[currLoc-1];
			array[currLoc-1] = temp;
			currLoc--;
		}
	}

	//print
	for (i=0; i<given_number;i++){
		printf("%d ", array[i]);
	}

	printf("\n");
	return 0;

}
