//LAB13 hw with struct and semaphores

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>


//pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

//double *a = NULL, sum = 0.0;
//int N, size;

typedef struct vars {
	pthread_t ptid; 
	double *a;
	double sum; //
	int N; //no of elements
	int size; //no threads
	int tid; //thread id 
} VARS;

void *compute(void *arg){
	VARS *info = (VARS*)arg;
	int myStart, myEnd, myN, i;
	//long tid = (long)arg;

	//determine start and end of computation for the current thread
	myN = info->N/info->size;
	myStart = info->tid*myN;
	myEnd = myStart + myN;
	if (info->tid == (info->size-1)) myEnd = info->N;

	//compute partial sum
	double mysum = 0.0;
	for (i = myStart; i < myEnd; i++){
		mysum += (double)i+1;
	}
//	printf("%f\n", mysum);
	//grab the loc, update global sum, and release loc
//	pthread_mutex_lock(&mutex);
	info->sum += mysum;
//	pthread_mutex_unlock(&mutex);

	return (NULL);
}

int main (int argc, char **argv){
	long i;
//	pthread_t *tid;
	VARS *info;

	if (argc != 3){
		printf("Usage: %s <# of elements> <#of threads>\n", argv[0]);
		exit(-1);
	}

	int N = atoi(argv[1]); //no elements
	int size = atoi(argv[2]); //no threads
	sem_t sem;
	int sem_init(&sem, 0, 1)
	
	//tid = (pthread_t*)malloc(sizeof(pthread_t)*size);
	//a = (double*)malloc(sizeof(double)*N);
	info = (VARS*)malloc(sizeof(VARS)*N);
	
	for (i = 0; i < size; i++){
		//a[i] = (double)(i+1);
		info[i].tid = i;
		info[i].N = N;
		info[i].size = size;
		info[i].sum = 0.0;
		info[i].a = NULL;
		pthread_create(&info[i].ptid, NULL, compute, (void*)&info[i]);		
//		printf("no. : %d", info[i].tid);
//		exit(0);
	}
//	printf("aok\n");
	double sum = 0.0;
	for (i = 0; i < size; i++){
		pthread_join(info[i].ptid, NULL);
		sum += info[i].sum;
	}
	
	free(info);
	printf("The total is %g, it should be equal to %g\n", sum, ((double)N*(N+1))/2);

	return 0;
}
