/* 
  Simple Pthread Program to find the sum of a vector.
  Uses mutex locks to update the global sum. 
  Author: Purushotham Bangalore
  Date: Jan 25, 2009

  To Compile: gcc -O -Wall pthread_psum.c -lpthread
  To Run: ./a.out 1000 4
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>


//pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;

#define min(a, b) (((a) < (b)) ? (a) : (b))

#define NBUFF 10
#define MAXNTHREADS 100

int nitems, nproducers, nconsumers;

struct {
	int buff[NBUFF];
	int nput;
	int nputval;
	int nget;
	int ngetval;
	sem_t mutex, nempty, nstored;
} shared;

//double *a=NULL, sum=0.0;
double sum = 0.0;
//int    N, size;

void *producer(void *), *consumer(void *);

/*
long compute(void *arg) {
    int myStart, myEnd, myN, i;
    long tid = (long)arg;

    // determine start and end of computation for the current thread
    myN = N/size;
    myStart = tid*myN;
    myEnd = myStart + myN;
    if (tid == (size-1)) myEnd = N;

    // compute partial sum
    double mysum = 0.0;
    for (i=myStart; i<myEnd; i++)
      mysum += a[i];

    // grab the lock, update global sum, and release lock
    pthread_mutex_lock(&mutex);
    sum += mysum;
    pthread_mutex_unlock(&mutex);

    return (NULL);
}
*/

int main(int argc, char **argv) {
    long i;	
    int prodcount[MAXNTHREADS], conscount[MAXNTHREADS];
    pthread_t tid_producer[MAXNTHREADS], tid_consumer[MAXNTHREADS];
    
//    pthread_t *tid;

    if (argc != 3) {
       printf("Usage: %s <# of elements> <# of threads>\n",argv[0]);
       exit(-1);
    }

//    N = atoi(argv[1]); // no. of elements
//    size = atoi(argv[2]); // no. of threads
    nitems = atoi(argv[1]);
    nproducers = min(atoi(argv[2]), MAXNTHREADS);
    nconsumers = min(atoi(argv[2]), MAXNTHREADS);

    sem_init(&shared.mutex, 0, 1);
    sem_init(&shared.nempty, 0, NBUFF);
    sem_init(&shared.nstored, 0, 0);

    

    // allocate vector and initialize
//    tid = (pthread_t *)malloc(sizeof(pthread_t)*size);
//    a = (double *)malloc(sizeof(double)*N); 
    
    // create producers
    for ( i = 0; i < nproducers; i++){
     	 prodcount[i] = 0;
	 pthread_create(&tid_producer[i], NULL, producer, (void *)i);
    }

    //create consumers
    for (i = 0; i < nconsumers; i++){
	conscount[i] = 0;
	pthread_create(&tid_consumer[i], NULL, consumer, (void *)i);
    }

    // wait for them to complete
    for ( i = 0; i < nproducers; i++)
      pthread_join(tid_producer[i], NULL);
    
    for (i = 0; i < nconsumers; i++){
      pthread_join(tid_consumer[i], NULL);
      sum += shared.buff[i];
    }
    sem_destroy(&shared.mutex);
    sem_destroy(&shared.nempty);
    sem_destroy(&shared.nstored);

    printf("The total is %g, it should be equal to %g\n", 
           sum, ((double)nitems*(nitems+1))/2);
    
    return 0;
}

void *producer(void *arg){
	int myStart, myEnd, myN, i;
	long tid = (long)arg;

	myN = nitems/nproducers;
	myStart = tid*myN;
	myEnd = myStart + myN;
	if (tid == (nproducers-1)) myEnd = nitems;

	double mysum = 0.0;
	for (i = myStart; i < myEnd; i++){
		mysum += (i+1);
	}

	for ( ; ; ){
		sem_wait(&shared.nempty);
		sem_wait(&shared.mutex);
	
		if (shared.nput >= nitems){
			sem_post(&shared.nstored);
			sem_post(&shared.nempty);
			sem_post(&shared.mutex);
			return(NULL);
		}
		shared.nputval = mysum;
		shared.buff[shared.nput % NBUFF] = shared.nputval;
		shared.nput++;
		shared.nputval++;

		sem_post(&shared.mutex);
		sem_post(&shared.nstored);
	}
}

void *consumer(void *arg){
	int i;
	for ( ; ; ){
		sem_wait(&shared.nstored);
		sem_wait(&shared.mutex);

		if(shared.nget >= nitems){
			sem_post(&shared.nstored);
			sem_post(&shared.mutex);
			return(NULL);
		}

		i = shared.nget % NBUFF;
		if (shared.buff[i] != shared.ngetval){
			printf("error: buff[%d] = %d\n", i, shared.buff[i]);
		}
		shared.nget++;
		shared.ngetval++;

		sem_post(&shared.mutex);
		sem_post(&shared.nempty);
				
	}
}

