/*
Stephen Scales
sgs44328
Project #1
*/

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
/*
References: Unix textbook and 4.22 code, pubs.opengroup.org/ --> references and examples of built in functions, 
previously provided code in lab and in class

*/

long int ARG_S;
char ARG_F[50];

typedef int MYFUNC(char *path, int indents);
/*
Establishes typedef to use function pointers for the different command line inputs
*/
int noinputs(char *path, int indents);
int argS(char *path, int indents);
int argF(char *path, int indents);
int pathFind(char *path, int indents, MYFUNC *f);
int both(char *path, int indents);
void print(char *temp, int indents);


int main (int argc, char *argv[]){
	char path[PATH_MAX];
	if (argc >5){
		printf("Issue with your input");
		return -1;
	}
	getcwd(path, sizeof(path));
	strcat(path, "/");
	int indents = 0;
	if (argc > 1){
		int i;
		for (i=1; i< argc; i++){
			if (strcmp(argv[i], "-s") == 0){
				ARG_S = atol(argv[i+1]);
				i++;
			}
			else if (strcmp(argv[i], "-f") == 0){
				strcpy(ARG_F, argv[i+1]);
				i++;
			}
			else {
				strcat(path, argv[1]);
				strcat(path, "/");
			}
		}
	}
	//tests to find which function pointer to use
	if (ARG_S != 0 && strlen(ARG_F) != 0){
		pathFind(path, indents, both);
	}
	else if (ARG_S != 0) {
		pathFind(path, indents, argS);
	}
	else if (strlen(ARG_F) > 0){
		pathFind(path, indents, argF);
	}
	else {
		pathFind(path, indents, noinputs);
	}

	return 0;

}

/*Recursive function that takes in function pointer used for printing */
int pathFind (char *path, int indents, MYFUNC *f){
	DIR *dir;
	struct stat buf;
	struct dirent *newdir;

	/* Base recursion step */
	if (lstat(path, &buf) < 0){
		//printf("lstat issue");
		f(path, indents);
		return 0;
	}
	if (S_ISDIR(buf.st_mode) == 0){
		f(path, indents); 	
		return 0;
	}
	int n = strlen(path);
	if (strcmp(&path[n-1], ".") != 0) {
		f(path, indents);
		dir = opendir(path);
		indents++;
		while ((newdir = readdir(dir)) != NULL){
			if (strcmp(newdir->d_name, ".") == 0 || strcmp(newdir->d_name, "..") == 0){
				continue;
			}
			char newStr[PATH_MAX];
			strcpy(newStr, path);
			strcat(newStr, newdir->d_name);
			strcat(newStr, "/");
			pathFind(newStr, indents, f);
		}
		closedir(dir);
	}
	return 0;
}

/*Function for if no user constrains given */
int noinputs (char *path, int indents){
	int i, len;
	printf("%s\n", path);
	len = strlen(path);
	printf("%d\n", len);
	char temp[200];
	char *name;
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if (strcmp(str, "/") == 0 && i+2 < len){//believe error to be here, how fn keeps printing after first iteration
			name = &path[i+1];
		}
		printf("%s\n", name);
	}
	strcpy (temp, name);
	print(temp, indents);
	return 0;
}

/*Function for a command line minimum size */
int argS(char *path, int indents){
	int i, len;
	struct stat buf;
	lstat(path, &buf);
	if (buf.st_size < ARG_S){
		return 0;
	}
	len = strlen(path);
	char temp[200];
	char *name;
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if (strcmp(str, "/") == 0 && i+2 < len){
			name = &path[i+1];
		}
	}
	strcpy(temp, name);
	print(temp, indents);
	return 0;
}

/*Function for a command line substring to test for */
int argF(char *path, int indents){
	int i, len;
	len = strlen(path);
	char temp[200];
	char *name;
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if (strcmp(str, "/") == 0 && i+2 < len){
			name = &path[i+1];
		}
	}
	strcpy(temp, name);
	if (strstr(temp, ARG_F) == NULL){
		return 0;
	}
	//printf("Path for below: %s\n", path);
	print(temp, indents);
	return 0;
}

/*Fuction for if both a size minimum and substring required */
int both(char *path, int indents){
	int i, len;
	struct stat buf;
	lstat(path, &buf);
	if (buf.st_size < ARG_S){
		return 0;
	}
	len = strlen(path);
	char temp[200];
	char *name;
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if (strcmp(str, "/") == 0 && i+2 < len){
			name = &path[i+1];
		}
	}
	strcpy(temp, name);
	if (strstr(temp, ARG_F) == NULL){
		return 0;
	}
	print(temp, indents);
	return 0;
}

/*Used as standard for printing with the right number of indentations */
void print(char *temp, int indents){
	int i, len;
	for (i=0; i < indents; i++){
		printf("\t");
	}
	len = strlen(temp);
	for (i=0; i <len-1; i++){
		printf("%c",temp[i]);
	}
	printf("\n");
}
