#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>


int ARG_S; 
char ARG_F[50];

typedef int fn (const struct stat *, int);
static fn func;
static char *fullpath;
static size_t pathlen;

static int pathFind(fn *, int);


int main (int argc, char *argv[]) {
	char path[PATH_MAX];
	if (argc > 5){
		printf("Issues with your input");
		return -1;
	}
	getcwd(path, sizeof(path));
	strcat(path, "/");

	if (argc > 1){
		//normal call but need to find current directory
		int i;
		for (i=1; i<argc; i++){
			//printf("%s\n", argv[i]); 
			if (strcmp(argv[i], "-s") == 0){
				ARG_S = atoi(argv[i+1]);
				i++;
			}
			else if (strcmp(argv[i], "-f") == 0){
				strcpy(ARG_F, argv[i+1]);
				i++;
			}
			else {
				strcat(path, argv[1]);
				strcat(path, "/");
			}
		
		}
	}
//	printf("%s\n", ARG_F);
//	printf("%d\n", ARG_S);
	fullpath = path; //malloc(*sizeof(char));
//	if (strlen(path) >= strlen(fullpath)){
//		pathlen = 2*strlen(path);
//		fullpath = realloc(fullpath, pathlen);
//	}
//	strcpy(fullpath, path);
//	printf("Fullpath %s\n",fullpath);
	int ret, indents;
	indents = 0;
	ret = pathFind(func, indents);
//	printf("Got this far");
	return 0;  
	

}

static int pathFind(fn *func1, int indents){
	DIR *dir;
	struct stat buf;
	struct dirent *newdir;
	int ret;


//	pathlen = strlen(fullpath);
	if (lstat(fullpath, &buf) < 0){
//		int i = lstat(fullpath, &buf);
//		printf("%d", i);
		printf("lstat error \n");
		return -1;
	} 
	if (S_ISDIR(buf.st_mode) == 0){
		//call to print file (non-directory)
//		printf("file, not dir");
		//exit(0);
		//char *name = {"h","i"};
		func(&buf, indents);
		return 1;
	}

	//now we know its a directory, so need to recursively call
	//pathlen *= 2;
	pathlen = strlen(fullpath);
//	exit(0);
	//fullpath = realloc(fullpath, pathlen);
	//strcpy(fullpath[n++], "/");
	//strcpy(fullpath[n], "0");
//	func(&buf, indents);
	int n = strlen(fullpath);
	if (strcmp(&fullpath[n-1], ".") != 0){
		
	
	
	//fullpath[n++] = '/';
//	fullpath[n++] = 0;
//	printf("pathFind fullpath %s", fullpath);
	dir = opendir(fullpath);
//	exit(0);
//	if ((dir = opendir(fullpath)) == NULL){
//		func(&buf, indents);
//		return 0;
//	}

//	func (&buf, indents);
	indents +=1;
	while ((newdir = readdir(dir)) != NULL){
		if (strcmp (newdir->d_name, ".") == 0 || strcmp(newdir->d_name,"..") == 0){
			continue;
		}
//		printf("%s\n", newdir->d_name);
		func(&buf, indents);
		strcpy(&fullpath[n], newdir->d_name);
//		printf("%s\n", fullpath);
//		indents += 1; //spacing?
		pathFind(func, indents); 
		if (ret != 0){
//			printf("ret ok");
//			exit(0);
		//	strcpy(&fullpath[n-1], "0");
		}
//		printf("indents %d : ", indents);
//		exit(0);
	}
//	fullpath[n-1] = 0;
//	strcpy(fullpath[n-1], "0");
	//fullpath[n-1] = 0;
	closedir(dir);
	return 0;
	}
	return 1;
		
	
}

int func(const struct stat *ptr, int indents){
	int i, len;
	len = strlen(fullpath);
	char temp[50];
//	printf("func\n");
	//exit(0);
	char* name;
	for (i=0; i < len; i++){
		char str[] = {fullpath[i]};
		if (strcmp(str, "/") == 0 && i+2 < len){
			name = &fullpath[i];
//			printf("%c\n", *name);
//			exit(0);
		}	
	}
	strcpy(temp, name);
	//if str contains global str here 
	//
	for (i=0; i<indents; i++){
		printf("\t");
	}
	printf("%s\n", temp);

	//change pathlen for recursive call so that truncates to prev dir
	strcpy(&fullpath[pathlen], "");
	printf("Full path in func:%s\n", fullpath);
	return 1;
}
 

