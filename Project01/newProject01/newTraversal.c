/*
Stephen Scales
sgs44328
Project #1
*/

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
/*
References: Unix textbook and 4.22 code, pubs.opengroup.org/ --> references and examples of built in functions, 
previously provided code in lab and in class

*/

long int ARG_S;
char ARG_F[100];

//typedef int MYFUNC(char *path, int indents);
/*
Establishes typedef to use function pointers for the different command line inputs
*/
//int noinputs(char *path, int indents);
//int argS(char *path, int indents);
//int argF(char *path, int indents);
typedef int MYFUNC (char *path, int indents, char *name);
int noinputs(char *path, int indents, char *name);
int argS(char *path, int indents, char *name);
int argF(char *path, int indents, char *name);

int pathFind(char *path, int indents, MYFUNC *f, char *name);
int both(char *path, int indents, char *name);
void print(char *temp, int indents);


int main (int argc, char *argv[]){
	char path[BUFSIZ];
	if (argc >5){
		printf("Issue with your input");
		return -1;
	}
	getcwd(path, sizeof(path));
	strcat(path, "/");
	int indents = 0;
	if (argc > 1){
		int i;
		for (i=1; i< argc; i++){
			if (strcmp(argv[i], "-s") == 0){
				ARG_S = atol(argv[i+1]);
				i++;
			}
			else if (strcmp(argv[i], "-f") == 0){
				strcpy(ARG_F, argv[i+1]);
				i++;
			}
			else {
				strcat(path, argv[1]);
				strcat(path, "/");
			}
		}
	}
	path[strlen(path)-1] = 0;
	//tests to find which function pointer to use
//	printf("%s\n",path);
	if (ARG_S != 0 && strlen(ARG_F) != 0){
//		printf("%ld %s\n", ARG_S, ARG_F);

		pathFind(path, indents, both, path);
	}
	else if (ARG_S != 0) {
//		printf("%ld\n", ARG_S);
		
		pathFind(path, indents, argS, path);
	}
	else if (strlen(ARG_F) > 0){
//		printf("%s\n", ARG_F);
		pathFind(path, indents, argF, path);
	}
	else {
		pathFind(path, indents, noinputs, path);
	}

	return 0;

}

/*Recursive function that takes in function pointer used for printing */
int pathFind (char *path, int indents, MYFUNC *f, char *name){
	DIR *dir;
	struct stat buf;
	struct dirent *newdir;
//	printf("level up\n");
//	fflush(stdout);
	/* Base recursion step */
	if (lstat(path, &buf) < 0){
		printf("lstat issue");
		return 0;
	}
	if (S_ISDIR(buf.st_mode) == 0){
//		printf("%s\n", path);
		f(path, indents, name); 	
		return 0;
	}
	int n = strlen(path);
	if (strcmp(&path[n-1], ".") != 0) {
		f(path, indents, name);
		dir = opendir(path);
		//printf("got to opendir");
		indents++;
		while ((newdir = readdir(dir)) != NULL){
			if (strcmp(newdir->d_name, ".") == 0 || strcmp(newdir->d_name, "..") == 0){
				continue;
			}
			char newStr[BUFSIZ];
			strcpy(newStr, path);
			strcat(newStr, "/");
			strcat(newStr, newdir->d_name);
			pathFind(newStr, indents, f, newdir->d_name);
		
		}
		closedir(dir);	
	}
	return 0;
}

/*Function for if no user constrains given */
int noinputs (char *path, int indents, char *name){
/*
//	printf("%s\n",path);
	int i, len;
	len = strlen(path);
//	char temp[200];
	char name[BUFSIZ];
	char str[1];
	for (i=0; i < len; i++){
	//	printf("%c\n", path[i]);
	//	strncpy(str, &path[i],1);
		char str[] = {path[i]};
	//	printf("%s\n", str);
		if ((strcmp(str, "/") == 0) && (i+1 < len)){
			//printf("%c\n", path[i]);
			//name = &path[i+1];
//			memset(name, 0, sizeof(name));
			//printf("%s\n", &path[i+1]);
			strcpy(name, &path[i+1]);
		}
	//	printf("%s\n",name);
	}
//	printf("%s\n",name);
	char *temp = malloc(strlen(name)*(sizeof(char)));
	strcpy (temp, name);
//	int i = 0;
//	int len = strlen(path);
//	char* token = strtok(path,"/");
//	char *tok;
//	while (token != NULL){
//		if (i == len) {continue;}
//		i++;
//		tok = token;
	//	printf("%s\n",token);
//		token = strtok(NULL, "/");
//	}	
	//printf("temp = %s name= %s\n", temp, name);
	//printf("aok\n");
//	char *temp;
	//strcpy(temp, token);
	//printf(tok);
	//exit(0);
//	temp = tok;
	//printf("aok\n");
	print(temp, indents);
	//printf("back from print\n");
//	free(temp);
*/	
	print(name, indents);
	return 0;
}

/*Function for a command line minimum size */
int argS(char *path, int indents, char *name){

//	printf("got to s\n");
//	int i, len;
	struct stat buf;
	lstat(path, &buf);
	//printf("%ld\n", buf.st_size);

	if (buf.st_size < ARG_S){
		//printf("%s\n", path);
		return 0;
	}
/*
	len = strlen(path);
	//char temp[200];
	char str[1];
	char name[BUFSIZ];
	//printf("---- %s\n",path);
	//printf("%d\n", len);
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if ((strcmp(str, "/") == 0) && (i+1 < len)){
			//name = &path[i+1];
			memset(name, 0, sizeof(name));
			strcpy(name, &path[i+1]);
		}
	}
//	char *temp = malloc(strlen(name)*(sizeof(char)));
	char temp[BUFSIZ];
	strcpy(temp, name);
	print(temp, indents);
	return 0;
*/
	print(name, indents);
	return 0;
}

/*Function for a command line substring to test for */
int argF(char *path, int indents, char *name){
/*
	int i, len;
	len = strlen(path);
	//char temp[200];
	char str[1];
	char name[BUFSIZ];
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if (strcmp(str, "/") == 0 && i+1 < len){
			//name = &path[i+1];
			memset(name, 0, strlen(name));
			strcpy(name, &path[i+1]);
			printf("%s\n", name);
		}
	}
	char *temp = malloc(strlen(name)*sizeof(char));
	//char temp[BUFSIZ];
	strcpy(temp, name);
//	printf("----------------%s\n", temp);
*/
	if (strstr(name, ARG_F) == NULL){
	//	printf("%s\n", temp);
		return 0;
	}
	print(name, indents);
/*
	//printf("Path for below: %s\n", path);
	print(temp, indents);
	//free(temp);
*/
	return 0;
}

	
/*Fuction for if both a size minimum and substring required */
int both(char *path, int indents, char *name){

//	int i, len;
	struct stat buf;
	lstat(path, &buf);
	if (buf.st_size < ARG_S){
		return 0;
	}
/*
	len = strlen(path);
	//char temp[100];
	char str[1];
	char name[BUFSIZ];
	for (i=0; i < len; i++){
		char str[] = {path[i]};
		if (strcmp(str, "/") == 0 && i+1 < len){
			//name = &path[i+1];
			strcpy(name, &path[i+1]);
		}
	}
	char *temp = malloc(strlen(name)*sizeof(char));
	strcpy(temp, name);
*/
	if (strstr(name, ARG_F) == NULL){
		return 0;
	}
	print(name, indents);

	return 0;
}

/*Used as standard for printing with the right number of indentations */
void print(char *temp, int indents){
//	printf("printed\n");
//	if (strcmp(temp, "newTraversal.c/") == 0){
//		exit(0);
//	}
	int i, len;
	for (i=0; i < indents; i++){
		printf("\t");
	}
//	printf("help\n");
	//exit(0);
	len = strlen(temp);
//	printf("%d\n", len);
	for (i=0; i <len; i++){
		printf("%c",temp[i]);
	}
	printf("\n");
//	printf("printed\n");
	//fflush(stdout);
	
	//free(temp);
	
}

