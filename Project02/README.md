BLAZER SHELL PROJECT:
This project implemented a basic shell in C that could run programs using
fork/exec, change directories, list all files in current directory, and 
exit on command. This project was implemented for a Systems Programming course.

Author:
Stephen Scales

Acknowledgements:
https://linux.die.net/man/3/execvp
https://www.geeksforgeeks.org/fseek-in-c-with-example/
https://www.tutorialspoint.com/c_standard_library/c_function_fopen.htm

Getting Started:
Use the Makefile to compile all the necessary files, then run the initial shell
calling ./fork 
This software uses two .h files altered from previous homeworks that both 
read/write to the log file and print all files within the current directory

Tests:
To run tests on this program, make the files first, then input ./fork, as listed
below, to start the shell. Follow the test cases below to test inputs 


./fork :                     should begin to run the shell program and a 
                            "blazersh>" should appear 

./binary :                    This will run the BinarySearch.c program which will
                            prompt the user for inputs, sort the inputs, and 
                            return the index of the a given number

cd testDir :                  change directory to the file within the test cases 
                            called testDir that contains hw1

./hw1 1000 :                  This will run the hw1 file provided in class on the 
                            input 1000 and print out the time for computation

list :                        Will list all files within the current directory

help :                        prints a list of all commands within blazersh

log :                         prints the total commands during that session of the 
                            command line (./binary \n ./hw1 1000)

quit :                        exits the shell 


Screenshots of Sample Session:

See included .png files within this repository

Contact Information:

Stephen Scales
sgs44328@uab.edu