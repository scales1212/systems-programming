#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "readdic.h"
#include "writelog.h"

int checkInput(char** input);

char** usrInput(char* line){
	char** tok = malloc(50*sizeof(char*));
	int i = 0;
	char* ptr;
	ptr = strtok(line, " ");
	while (ptr != NULL){
		tok[i] = ptr;
		i++;
		ptr = strtok(NULL, " ");
	}
	return tok;
}

int main (int argc, char **argv){

	pid_t pid;
	int status; 
	
	while (1){
		char** input = malloc(50 * sizeof(char*));
		char* message = {"blazersh> "};
		char line[50];

		printf("%s", message);

		fgets(line, 50, stdin);
		line[strlen(line)-1] ='\0';
		input = usrInput(line);
		int check = checkInput(input);

		if (check == 1){
			loginput(input);				
			pid = fork();
			if (pid == 0){ //child
				execvp(input[0], &input[0]);
				printf("If you see this statement then execvp failed\n");
				exit(-1);
			}
			else if (pid > 0){ //parent
				wait(&status); 
				if (WIFEXITED(status)){
					//printf("Child process exited with status = %d\n", WEXITSTATUS(status));
				}
				else {
					printf("Child process did not terminate normally: %d\n", status);
				}
			}
			else {
				perror("fork");
				exit (EXIT_FAILURE);
			}
		}
		if (check == 2){
			//change directory
			chdir(input[1]);
			
		}
	}
	return 0;
}

/* Function checks user input within blazersh and, based on the input, decides what the shell should do next */
int checkInput(char **input){
	char *var1 = input[0];
	if (strcmp(var1, "list") == 0){
		//call readdic
		char *current = {"./"};
		readdic(current);
		return 0;
	}
	else if (strcmp(var1, "quit") == 0){
		fclose(fp);
		removelog();
		exit(0);
		return 0;
	}
	else if(strcmp(var1, "cd") == 0){
		return 2;
	}
	else if (strcmp(var1, "help")== 0){
		printf("\n");
		printf("\t\tHELP COMMAND MENU\n");
		printf("\tlist - lists all files in current directory\n");
		printf("\tcd - changes current directory\n");
		printf("\tlog - prints out complete list of commands in given session\n");
		printf("\tquit - exits the shell\n");
		printf("\thelp - brings up this help menu\n");
		printf("\n");
		return 0;
	}
	else if (strcmp(var1, "log") == 0){
		printlogs();
		return 0;
	}
	else {return 1;}
}
