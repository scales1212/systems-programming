#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

int readdic (char *argv){
	struct dirent *dirent;
	DIR *parentDir;

//	if (argc < 2){
//		printf("Usage: %s <dirname>\n", argv[0]);
//		exit(-1);
//	}
	
	parentDir = opendir(argv);
	if (parentDir == NULL){
		printf("Error opening directory '%s'\n", argv);
		exit(-1);
	}

	int count = 1;
	while ((dirent = readdir(parentDir)) != NULL){
		if (strcmp((*dirent).d_name, ".") == 0 || strcmp((*dirent).d_name, "..") == 0){
			continue;
		}
		printf("%s\n", (*dirent).d_name);
		count++;
	}

	closedir(parentDir);
	return 0;
}
