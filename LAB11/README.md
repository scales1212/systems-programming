This is the file for Lab11 that focused on pipes. This assignment asked us to 
implement a single pipe within a while loop in order to handle UNIX calls. 
This implementation utilizes popen and pclose to take in the user command, process
it, then display the results within a while loop acting like a shell. 
A sample input would be:
./lab11
Command: ls *.c
pipe1.c pipe2.c pipe3.c 

Author:
Stephen Scales
sgs44328@uab.edu