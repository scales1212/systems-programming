/*simple program that takes in three commands and uses fork/exec and pipes to run all 3
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(int argc, char **argv){
	pid_t pid1, pid2, pid3;
	int pipefd1[2];
	int pipefd2[2];
	int status1, status2, status3;

	if (argc != 4){
		printf("Usage: %s <command1> <command2> <command3>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if (pipe(pipefd1) != 0){
		perror("pipe");
		exit(EXIT_FAILURE);
	}

	if (pipe(pipefd2) != 0){
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	
	pid1 = fork();
	if(pid1 == 0){
		close(pipefd1[0]);
		close(pipefd2[0]);
		close(pipefd2[1]);

		if (dup2(pipefd1[1], 1) == -1){
			perror("dup2");
			exit(EXIT_FAILURE);
		}
		execlp(argv[1], argv[2], (char*)NULL);
		perror("execlp");
		exit(EXIT_FAILURE);
	}
	else if(pid1 < 0){
		perror("fork");
		exit(EXIT_FAILURE);
	}
	
	pid2 = fork();
	if(pid2 == 0){
		close(pipefd1[1]);
	
		if (dup2(pipefd1[0], 0) == -1){
			perror("dup2");
			exit(EXIT_FAILURE);
		}
		close(pipefd2[0]);
		if (dup2(pipefd2[1], 1) == -1){
			perror("dup2");
			exit(EXIT_FAILURE);
		}
		
		execlp(argv[2], argv[2], (char*)NULL);
		perror("execlp");
		exit(EXIT_FAILURE);
	}
	else if(pid2 < 0){
		perror("fork");
		exit(EXIT_FAILURE);
	}

	pid3 = fork();
	if (pid3 == 0){
		close(pipefd1[0]);
		close(pipefd1[1]);
		close(pipefd2[1]);

		if (dup2(pipefd2[0], 0) == -1){
			perror("dup2");
			exit(EXIT_FAILURE);
		}
		execlp(argv[3], argv[3], (char*)NULL);
		perror("execlp");
		exit(EXIT_FAILURE);
	}
	else if(pid3 < 0){
		perror("fork");
		exit(EXIT_FAILURE);
	}
	close(pipefd1[0]);
	close(pipefd1[1]);
	close(pipefd2[0]);
	close(pipefd2[1]);

	waitpid(pid1, &status1, 0);
	waitpid(pid2, &status2, 0);
	waitpid(pid3, &status3, 0);

	return 0;
}
