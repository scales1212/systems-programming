/*Example program where parent creates 2 children and pipes them
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main (int argc, char **argv){
	pid_t pid1, pid2;
	int pipefd[2];
	int status1, status2;

	if (argc != 3){
		printf("Usage: %s <command1> <command2>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if (pipe(pipefd) == 0){
		pid1 = fork();
		if (pid1 == 0){
			//child 1
			close(pipefd[0]);
		
			if(dup2(pipefd[1], 1) == -1){
				perror("dup2");
				exit(EXIT_FAILURE);
			}
			/*execute command1*/
			execlp(argv[1], argv[1], (char*)NULL);
			printf("If you see this statement then exec failed\n");
			perror("execlp");
			exit(EXIT_FAILURE);
		}
		else if (pid1 < 0){
			perror("fork");
			exit(EXIT_FAILURE);
		}
		
		/*second fork*/
		pid2 = fork();
		if (pid2 == 0){
			close(pipefd[1]);
			
			if(dup2(pipefd[0], 0) == -1){
				perror("dup2");
				exit(EXIT_FAILURE);
			}
			
			execlp(argv[2], argv[2], (char*)NULL);
			printf("If you see this exec failed\n");
			perror("execlp");
			exit(EXIT_FAILURE);
		}
		else if(pid2 < 0){
			perror("fork");
			exit(EXIT_FAILURE);
		}

		close(pipefd[0]);
		close(pipefd[1]);
		waitpid(pid1, &status1, 0);
		waitpid(pid2, &status2, 0);
	}
	else {
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	return 0;
}
