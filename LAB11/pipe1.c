/*Simple program takes input from user and passes it to child process to
 *convert to upper case then return on command line
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main (int argc, char **argv){
	pid_t pid;
	int status;
	int pipefd[2];
	char c;

	if (argc != 2){
		printf("Usage: %s <sting>\n", argv[0]);
		exit(-1);
	}
	if (pipe(pipefd) == 0){
		if ((pid = fork()) == 0){
			//child procees
			close(pipefd[1]);
		
			while (read(pipefd[0], &c, 1) > 0){
				c = toupper(c);
				write(1, &c, 1);
			}
			write (1, "\n", 1);
			close(pipefd[0]);
			exit(EXIT_SUCCESS);
		}
		else if (pid > 0) {
			//parent process
			close(pipefd[0]);
			write(pipefd[1], argv[1], strlen(argv[1]));
			close(pipefd[1]);

			wait(&status);
			if (WIFEXITED(status)){
				printf("child process exited with status = %d\n", WEXITSTATUS(status));
			}
			else {
				printf("child process did not terminate normally\n");
			}
		}
		else {
			perror("fork");
			exit(EXIT_FAILURE);
		}
	}
	else {
		perror("pipe");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}
