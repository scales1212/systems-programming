/*LAB11: takes in UNIX commands and executes the command using popen and pclose
 *When finished, prompts user for another command unit quit is sent
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
/*
char** usrInput(char *line){
	char **tok = malloc(50*sizeof(char*));
	int i = 0;
	char* ptr;
	ptr = strtok(line, " ");
	while (ptr != NULL){
		tok[i] = ptr;
		i++;
		ptr = strtok(NULL, " ");
	}
	return tok;
}
*/
int main (int argc, char **argv){
	FILE *fp1; //, *fp2;
	char line[BUFSIZ];
	/*
	if (argc != 3){
		printf("Usage: %s <command1> <command2>\n", argv[0]);
		exit(EXIT_FAILURE);
	}*/

	while (1) {
		printf("Enter Command: ");
		char inputline[BUFSIZ];

		fgets(inputline, 50, stdin);
//		inputline[strlen(inputline)-1] = '\0';
//		char **input = malloc(50*sizeof(char*));
//		input = usrInput(inputline);
		
		if (strstr(inputline, "quit")){
			exit(0);
		}
		//can run inputs
		if ((fp1 = popen(inputline, "r")) == NULL){
			perror("popen");
			exit(EXIT_FAILURE);
		}
		/*
		if ((fp2 = popen(input[1], "w")) == NULL){
			perror("popen");
			exit(EXIT_FAILURE);
		}
		*/
//		printf("------\n");
//		exit(0);	
		while (fgets(line, BUFSIZ, fp1) != NULL){
			if (fputs(line, stdout) == EOF){
				exit(EXIT_FAILURE);
			}
		}
		if (pclose(fp1) == -1){// || pclose(fp2) == -1){
			perror("pclose");
			exit(EXIT_FAILURE);
		}
	}
	return 0;
}
