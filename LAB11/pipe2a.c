/*example that uses popen and pclose to create new pipes
 *
 */

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char **argv){
	FILE *fp1, *fp2;
	char line[BUFSIZ];

	if (argc != 3){
		printf("Usage: %s <command1> <command2>\n", argv[0]);
		exit(EXIT_FAILURE);
	}

	if ((fp1 = popen(argv[1], "r")) == NULL){
		perror("popen");
		exit(EXIT_FAILURE);
	}

	if ((fp2 = popen(argv[2], "w")) == NULL){
		perror("popen");
		exit(EXIT_FAILURE);
	}

	while (fgets(line, BUFSIZ, fp1) != NULL){
		if (fputs(line, fp2) == EOF){
			exit(EXIT_FAILURE);
		}
	}
	if ((pclose(fp1) == -1) || pclose(fp2) == -1){
		perror("pclose");
		exit(EXIT_FAILURE);
	}
	return 0;
}
