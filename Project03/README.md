BLAZER SHELL PROJECT:
This project implemented a basic shell in C that could run programs using
fork/exec, change directories, list all files in current directory, and 
exit on command. Furthermore, this shell can take in < > symbols to redirect the 
child process as well as a way to use SIGNAL to stop the child process and 
use jobs to print out the stopped job. One can also use continue <pid> to continue
the process after stopping. 
This project was implemented for a Systems Programming course.

Author:
Stephen Scales

Acknowledgements:
https://linux.die.net/man/3/execvp
https://www.geeksforgeeks.org/fseek-in-c-with-example/
https://www.tutorialspoint.com/c_standard_library/c_function_fopen.htm

Getting Started:
Use the Makefile to compile all the necessary files, then run the initial shell
calling ./fork 
This software uses two .h files altered from previous homeworks that both 
read/write to the log file and print all files within the current directory

Tests:
To run tests on this program, make the files first, then input ./fork, as listed
below, to start the shell. Follow the test cases below to test inputs 

./fork :                     should begin to run the shell program and a 
                            "blazersh>" should appear 

./hello :                     Prompts user for their name then prints Hello + the
                                name 

./hello < input.txt > out.txt: Takes the name from the previously made file input.txt
                              and prints Hello + that name in out.txt
                              
./hello < input.txt :         Takes the name from the previously made file input.txt
                              and prints Hello + that name to the terminal

./hw2 500 :
^Z                            Running hw2 500 then pressing control Z to stop the 
                              process sends the stop signal to the child and 
                              returns the user to the command line 

jobs :                        prints out all stopped processes with their pid 
                              and the function name 
                              
continue <pid> :              continues the process that was stopped 

list :                        Will list all files within the current directory

help :                        prints a list of all commands within blazersh

log :                         prints the total commands during that session of the 
                            command line (./binary \n ./hw1 1000)

quit :                        exits the shell 

Screenshots of Sample Session:

See included .png files within this repository

Contact Information:

Stephen Scales
sgs44328@uab.edu