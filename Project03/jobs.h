#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

FILE *fp, *fp2;
void stopChild(int pid, char *childname){
	fp = fopen("jobs.txt", "a");
	fseek(fp, 0, SEEK_END);
	fprintf(fp, "%d \t %s\n", pid, childname);
	fclose(fp);
}

void continueChild(char *pid){
	fp = fopen("jobs.txt", "r");
	fp2 = fopen("jobs2.txt", "a");

//	char spid[50];
//	sprintf(spid, "%d", pid);
	char line[BUFSIZ];
	while (fgets(line, BUFSIZ, fp) != NULL){
		if (strstr(line, pid) == NULL ){
			fprintf(fp2,"%s", line);				
		}
	}
	fclose(fp);
	fclose(fp2);
	remove("jobs.txt");
	rename("jobs2.txt", "jobs.txt");
	int p = atoi(pid);
	kill(p, SIGCONT);
}
void printProcesses(){
	printf("PID \t PROCESS\n");
	fp = fopen("jobs.txt", "r");

	char line[BUFSIZ];
	while (fgets(line, BUFSIZ, fp) != NULL){
		printf("%s\n", line);
	}
	fclose(fp);
}
void removeJobFile(){
	remove("jobs.txt");
}

