#include <stdio.h>
#include <stdlib:.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include "readdic.h"
#include "writelog.h"
#include "redirect.h"
#include "jobs.h"

pid_t pid;
char childname[50];
int checkInput(char** input);
int suspend = 0;
int fdin, fdout;
/*signal functions to handle both parent and child interrupt signals from user 
 *
 */
static void sig_child(int signo){
	int status;

	signal (signo, SIG_IGN);
//	printf("-!-!-\n");
	waitpid(pid, &status, WNOHANG | WUNTRACED);

	if (WIFEXITED(status)){
		printf("child exited w/ status = %d\n", WEXITSTATUS(status));
	}
	else if(WIFSTOPPED(status)){
		printf("success\n");
		stopChild(pid, childname);	
	}
	else {
		printf("child did not exit normally\n");
	}

//	while (!WIFEXITED(status) && !WIFSIGNALED(status)){continue;}
	
//	if (WIFEXITED(status)){printf("status exit= %d\n", WEXITSTATUS(status));}
//	else {printf("didnt exit normally\n");}
	fflush(stdout);
	signal(signo, sig_child);
}

static void sig_int(int signo){
	switch(signo){
		case SIGINT:
			kill(pid, SIGINT);	
			printf("\nchild killed by user command ^C\n");
			break;
		case SIGTSTP:
			suspend = 1;
			kill(pid, SIGTSTP);
			//printf("-----%s\n", childname);
			stopChild(pid, childname);
			printf("\nProcess with pid=%d is stopped\n", pid);
			break;
		case SIGQUIT:
			exit(0);
		default: 
			printf("received signal %d\n", signo);
	}
}


char** usrInput(char* line){
	char** tok = malloc(50*sizeof(char*));
	int i = 0;
	char* ptr;
	ptr = strtok(line, " ");
//	printf("made before ptr\n");
	while (ptr != NULL){
		tok[i] = ptr;
		i++;
		ptr = strtok(NULL, " ");
	}
	return tok;
}

int main (int argc, char **argv){

	//pid_t pid;
	int status; 
	
	while (1){
		char** input = malloc(50 * sizeof(char*));
		char* message = {"blazersh> "};
		char line[50];

		printf("%s", message);

		fgets(line, 50, stdin);
//		printf("fgets\n");
//		exit(0);
		line[strlen(line)-1] ='\0';
		input = usrInput(line);
//		printf("%s\n", input[0]);
//		exit(0);
		int check = checkInput(input);
//		printf("aok\n");
//		exit(0);
		if (check == 1){
			loginput(input);				
			strcpy(childname, input[0]);
			pid = fork();

			if (pid == 0){ //child
//				strncpy(childname,input[0], 50);
//				printf("%s\n", childname);
				fdin = checkInFile(input);
				fdout = checkOutFile(input);
				redirect(fdin, fdout);
				
				int pos;
				pos = getPos(input);
//				printf("--------%d\n",pos);
				
				if (pos != 0){ 
					input[pos] = (char*)NULL;
				}
//				printf("%s\n", input[0]);			
				execvp(input[0], &input[0]);
				printf("If you see this statement then execvp failed\n");
				exit(-1);	
			}

			else if (pid > 0){ //parent
				if (signal(SIGINT, sig_int) == SIG_ERR){
					printf("unable to catch SIGINT\n");
				}
				if (signal(SIGTSTP, sig_int) == SIG_ERR){
					printf("unable to catch SIGTSTP\n");
				}
				if (signal(SIGQUIT, sig_int) == SIG_ERR){
					printf("unable to catch SIGQUIT\n");
				}
				//waitpid(-1, &status, WUNTRACED);
				
				//waitpid(pid, &status, 0); 
				
				waitpid(-1, &status, WUNTRACED);
				closeRedirect(fdin, fdout);
			}

			else {
				perror("fork");
				exit (EXIT_FAILURE);
			}
		}
		if (check == 2){
			//change directory
			chdir(input[1]);
			
		}
	}
	return 0;
}

/* Function checks user input within blazersh and, based on the input, decides what the shell should do next */
int checkInput(char **input){
	char *var1 = input[0];
	if (strcmp(var1, "list") == 0){
		//call readdic
		char *current = {"./"};
		readdic(current);
		return 0;
	}
	else if (strcmp(var1, "quit") == 0){
		fclose(fp);
		removeJobFile();	
		removelog();
		exit(0);
		return 0;
	}
	else if(strcmp(var1, "cd") == 0){
		return 2;
	}
	else if (strcmp(var1, "help")== 0){
		printf("\n");
		printf("\t\tHELP COMMAND MENU\n");
		printf("\tlist - lists all files in current directory\n");
		printf("\tcd - changes current directory\n");
		printf("\tlog - prints out complete list of commands in given session\n");
		printf("\tcontinue - continues given pid given along with coninue\n");
		printf("\tjobs - prints out list of suspended jobs\n");
		printf("\tquit - exits the shell\n");
		printf("\thelp - brings up this help menu\n");
		printf("\n");
		return 0;
	}
	else if (strcmp(var1, "log") == 0){
		printlogs();
		return 0;
	}
	else if(strcmp(var1, "continue") == 0){
		continueChild(input[1]);
		printf("Process %s started\n", input[1]);
		suspend = 0;
		return 0;
	}
	else if (strcmp(var1, "jobs") == 0){
		printProcesses();
		return 0;
	}
	else {return 1;}
}
