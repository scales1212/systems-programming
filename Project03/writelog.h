#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define LINESIZE 1024

FILE *fp; 

void loginput(char **command){
	fp = fopen("blazersh.log", "a");
	fseek(fp, 0, SEEK_END);
	fprintf(fp, "%s\n", *command);
	fclose(fp);
}

void printlogs(){
	fp = fopen("blazersh.log", "r");

	char line[LINESIZE];
	while (fgets(line, LINESIZE, fp) != NULL){
		printf("%s", line);
	}

	fclose(fp);
}

void removelog(){
	remove("blazersh.log");
}
