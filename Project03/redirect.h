#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>


int fdin, fdout;
//FILE *fpin;
int checkInFile(char **input){
	//FILE* in = stdin;
	fdin = 0;
	int i = 0;
//	len = strlen(*input);
//	printf("%d\n", len);	
//	for(i=0; i < len; i++){
//		printf("%s\n", input[i]);
	while (input[i] != NULL){
		if (strcmp(input[i], "<") == 0){
//			printf("yes\n");
//			exit(0);
			char in[50];
//			in = input[i+1];
//			memcpy(in, input[i+1], strlen(input[i+1]));
			
			strcpy(in, input[i+1]);
//			int l = strlen(in);
//			in[l] = '\0';
//			fdin = open("./input.txt", O_RDONLY);
			//printf("got here\n");
			//printf("%s\n", in);
			if((fdin = open(in, O_RDONLY)) == -1){
				printf("error opening child new stdin\n");
				exit(EXIT_FAILURE);
			}
		}
		i++;
	}
//	printf("yes\n");
//	exit(0);
	return fdin;
}

int checkOutFile(char **input){
	//FILE* out = stdout;
	fdout = 0;
	int i = 0;
//	len = strlen(*input);

//	for(i=0; i < len; i++){
	while (input[i] != NULL){
		if (strcmp(input[i], ">") == 0){
			char out[50];
			strcpy(out, input[i+1]);
			//int l = strlen(out);
			//out[l] = 0;
			if((fdout = open(out, O_CREAT | O_APPEND | O_WRONLY, 0777)) == -1){
				printf("error opening child new stdout\n");
				exit(-1);
			}
		}
		i++;
	}
	return fdout;
}

int redirect(int fdin,int fdout){
	if (fdin > 0) {
		dup2(fdin, 0);
	}
	if(fdout > 0){
		dup2(fdout,1);
	}
	return 0;
}

int getPos(char **input){
	int i = 0;
	while (input[i] != NULL){
		if (strcmp(input[i], ">") == 0 || strcmp(input[i], "<") == 0){
//			printf("%s\n", input[i]);
//			printf("int getPos: %d", i);
			return i;
		}
		i++;
	}
	return i;
}
void closeRedirect(int fdin, int fdout){
//	printf("%d\n", fdin);
	if (fdin > 0){
		close(fdin);
	}
	if (fdout > 0){
		close(fdout);	
	}
}
