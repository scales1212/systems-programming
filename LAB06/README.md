This program takes the given csv file and, using structs, reads in the 
data and creates two new files: one is sorted by the host name and the other 
is sorted by the price. Use the Makefile to compile and make sure the given csv
is in the tar. The program will create both the new files when it writes to them.
As per the instructions, nothing is printed but all the sorted data is found 
in the new documents. 