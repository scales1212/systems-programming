#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char **argv){
	pid_t pid;
	int status;

	pid = fork();
	if (pid == 0){
		printf("This is the child process, my PID is %ld and my parent PID is %ld\n", (long)getpid(), 
			(long)pid);
	}
	else if (pid > 0){
		printf("This is the parent process, my PID is %ld and the child PID is %ld\n", (long)getpid(),
			(long)pid);
		printf("Wait for the child process to terminate\n");
		wait(&status);

		if (WIFEXITED(status)){
			printf("Child process exited with status = %d\n", WEXITSTATUS(status));
		}
		else {
			printf("ERROR: Child process did not terminate normally!\n");
		}
	}
	else {
		perror("fork");
		exit(EXIT_FAILURE);
	}

	printf("[%ld]: Exiting program .....\n", (long)getpid());
	return 0;
	
}
