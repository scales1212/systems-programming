#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <time.h>
#include <string.h>

#define LINESIZE 1024

int main (int argc, char **argv){
	pid_t pid;
	int status;
	
	if (argc < 2) {
		printf("Usage: %s <command> [args]\n", argv[0]);
		exit(-1);
	}
	
	FILE *fp = fopen(argv[1], "r");
	FILE *np = fopen("output.log", "wb");
	if (fp == NULL){
		perror("Unable to open\n");
		exit(-1);
	}
	
	char line[BUFSIZ];
	while (fgets(line, BUFSIZ, fp) != NULL){
		time_t begin = time(NULL);
		int len = strlen(line);
		line[len-1] = '\0';
		pid = fork();
		if (pid == 0){
			execvp(line, &line[0]);
			printf("If you see this then execvp failed\n");
			exit(-1);
		}
		else if (pid > 0){
			printf("Wait for child\n");
			wait(&status);
			time_t end = time(NULL);
			if (WIFEXITED(status)){
				printf("Child process exited with status =%d\n", WEXITSTATUS(status));
				time_t tot = end - begin;
				char b[50], e[50];
				strcpy(b, ctime(&begin));
				strcpy(e, ctime(&end));
				printf("child proc time %s\n", ctime(&tot));
			//	int len = strlen(line);
				int len1 = strlen(b);
				int  len2 = strlen(e);
			//	line[len] = '\0';
				b[len1] = '\0';
				e[len2] = '\0';
				
				fprintf(np, "%s \t %s \t %s", line, b, e);
			}
			else{
				printf("Child process didnt terminate normally\n");
			}
		}
		else {
			perror("fork");
			exit(EXIT_FAILURE);
		}	
	}
	printf("Exiting program and all good\n");
	
	fclose(fp);
	fclose(np);	
}
