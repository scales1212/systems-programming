This is the lab 07 project for implementing exec and fork. 
Although many of the tasks outlined in the document are completed in 
forkexecvp.c, it does not perform all the functionality and the time is not 
precise enough to show any differences. 
A tar file is included with all the relevant files, as well as a Makefile 
