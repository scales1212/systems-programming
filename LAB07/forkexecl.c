#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main (int argc, char **argv){
	pid_t pid;
	int status;

	pid = fork();
	if (pid == 0){
		execl("/bin/uname", "uname", "-a", (char *)NULL);
		printf("If you see this statement then execl failed\n");
	}
	else if (pid > 0) {
		printf("Wait for the child process to terminate\n");
		wait (&status);
		if (WIFEXITED(status)){
			printf("Child process exited with status = %d\n", WEXITSTATUS(status));
		}
		else {
			printf("Child process did not terminate normally!");
		}
	}
	else {
		perror("fork");
		exit(EXIT_FAILURE);			
	}

	printf("[%ld]: Exiting program.......\n",(long)getpid());
	return 0;
}
