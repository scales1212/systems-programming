This is the repository for lab12 for CS332 Systems Programing. The purpose of this
assignment was to implement threads and mutexes in a simple summation calculation.
pthread_sum.c was written to use structs to pass data in thread create instead
of global variables. A sum variable is initiated in main so that the program 
can sum each total value from the different threads to return a total. 
Code is based on provided programs on threads by the professor. 

Contact:
Stephen Scales 
sgs44328@uab.edu