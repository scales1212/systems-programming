//simple Pthread program that takes in struct

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct foo {
	pthread_t pid; //thread id returned by pthread_create
	int tid; // user managed thread id 
	int nthreads; //total no threads created
} FOO;

void *compute(void *args){
	FOO *info = (FOO*)args;
	printf("Hello, I am thread %d of %d\n", info->tid, info->nthreads);
	return (NULL);
}

int main(int argc, char **argv){
	int i, nthreads;
	FOO *info;

	if(argc != 2){
		printf("Usage: %s <# of threads>\n", argv[0]);
		exit(-1);
	}

	nthreads = atoi(argv[1]);

	info = (FOO*)malloc(sizeof(FOO)*nthreads);

	for (i = 0; i < nthreads; i++){
		info[i].tid = i;
		info[i].nthreads = nthreads;
		pthread_create(&info[i].ptid, NULL, compute, (void*)&info[i]);
	}
	
	for (i = 0; i < nthreads; i++){
		pthread_join(info[i].ptid, NULL);
	}

	free(info);
	printf("Exiting main program\n");
	
	return 0;
}


