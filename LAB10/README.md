The Lab10 project focused on I/O redirection and changing child processes in 
fork/exec operations to write standard out and standard error to different files
than the parent. This required ammending the Lab7 project to allow for this 
behavior. The TA provided the lab7 solution, therefore all that was required was
to change the code slightly to create and write to different files, whose names 
were based on the process id of the child. 

To run, use the make file to compile the code and include the stdin.txt file as
seen below:
./lab10 stdin.txt

The stdin.txt contains the simple calls for the child processes to complete 


Written by Stephen Scales : sgs44328@uab.edu
