Job Scheduler Project: 
This project creates a job scheduler that takes in commands
and runs/manages as many projects as the user would like. The program utitlizes 
fork/exec and jumps back to the parent after adding any new processes to the job
queue, therefore acting somewhat like a shell. The user can either send in a new
command using submit + the executable to run or send showjobs which prints out
all current processes and whether they are running or not. Any stdout or stderr
messages are redirected to new files that are titled jobid.err/jobid.out. The program 
initially takes in the max number of processes the computer can run at once as a command
line argument, so, if the scheduler has more processes sent to it than the computer
can handle at that time, it categorizes the later processes as waiting while the 
rest are running. The scheduler has a built in queue that manages all current 
processes. 
This project was implemented for a Systems Programming course. 

Acknowledgments: 
https://linux.die.net/man/3/execvp, https://linux.die.net/man/2/waitpid, and 
the provided code by the professor 

Getting Started: 
Use the Makefile by calling make to compile scheduler.c and
jobqueue.c, then run ./scheduler along with the max number of processes (typically
less than 8)

Tests:
To run tests on this program, make the files first, then input ./scheduler 2, as listed
below, to start the shell. Follow the test cases below to test inputs

./scheduler 2 :             Should start the program with 2 as the max number of 
current processes 

submit ./hw1 2000 :         Runs hw1 and jumps back to the "outer shell". All
computed values printed to jobid+.out

submit ./hw1 2000 :         Runs hw1 as second process and jumps back to outer 
shell, new .out file created with its jobid

submit ./hw1 1000 :         Runs hw1 as third process and jumps back to outer shell

showjobs :                  Prints the table showing all current jobs and their 
status, jobid 2 (third process) will have waiting under its status 

Screenshot:
See attached .png files

Contact Information:
Stephen Scales
sgs44328@uab.edu
