/* Implementation of a simple circular queue using a static array */
#include <stdio.h>
#include <stdlib.h>
#include "queueh.h"
#include <string.h>

/* create the queue data structure and initialize it */
queue *queue_init(int n) {
	queue *q = (queue *)malloc(sizeof(queue));
	q->size = n;
	q->buffer = (char*)malloc(sizeof(char*)*n);
	q->start = 0;
	q->end = 0;
	q->count = 0;

	return q;
}

/* insert an item into the queue, update the pointers and count, and
   return the no. of items in the queue (-1 if queue is null or full) */
int queue_insert(queue *q, char* item) {
	if ((q == NULL) || (q->count == q->size))
	   return -1;

	strcpy(&q->buffer[q->end *sizeof(char*)], item);//end %q->size	
//	printf("%s\n", &q->buffer[q->end]);
//	q->buffer[q->end % q->size] = item //end % q->size
	q->end = (q->end + 1);
	q->count++;

	return q->count;
}

/* delete an item from the queue, update the pointers and count, and 
   return the item deleted (-1 if queue is null or empty) */
int queue_delete(queue *q) {
	if ((q == NULL) || (q->count == 0))
	   return -1;

	//int x = q->buffer[q->start];
	q->start = (q->start + 1);
	q->count--;

	return q->count; //x;
}

/* display the contents of the queue data structure 
void queue_display(queue *q) {
	int i;
	if (q != NULL && q->count != 0) {
		printf("queue has %d elements, start = %d, end = %d\n", 
			q->count, q->start, q->end);
		printf("queue contents: ");
		for (i = 0; i < q->count; i++)
	    		printf("%d ", q->buffer[(q->start + i) % q->size]);
		printf("\n");
	} else
		printf("queue empty, nothing to display\n");
}
`*/

/*display jobs & take in P to decide if running or waiting*/
void queue_jobs(queue *q, int P){
	int i;
	int len = q->count;// + 1;
//	printf("%d", len);
	printf("jobid \t command \t status\n");
//	if(len > 1){
		for (i = 0; i < len; i++){
			printf("%d \t %s",i, &q->buffer[(q->start+i)* sizeof(char*)]); //% q->size]);
			if (i < P){
				printf("\t\tRunning\n");
			}
			else{
				printf("\t\tWaiting\n");
			}
		}
//	}
}


/* delete the queue data structure */
void queue_destroy(queue *q) {
	free(q->buffer);
	free(q);
}

