#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <fcntl.h>
#include "queueh.h"


pid_t pid;
int P; // max number of jobs able to process
int fdout, ferr; //used for redirect
queue *q;
//q = queue_init(10);
int redirect();

static void sig_child(int signo);
char **usrInput(char* line){
	char **tok = malloc(50*sizeof(char*));
	int i = 0;
	char* ptr;
	ptr = strtok(line, " ");
	while (ptr != NULL){
		tok[i] = ptr;
		i++;
		ptr = strtok(NULL, " ");
	}
	return tok;
}

int main (int argc, char **argv){
	int status;
//	queue *q;
	if (argc < 1){
		printf("Usage Error\n");
		exit(-1);
	}
	P =  atoi(argv[1]);
	q = queue_init(10);

	while (1){
		char **input = malloc (50*sizeof(char*));
		char *message = {"Enter Command> "};
		char line[50];
		memset(line, 0, 50);
		
		printf("%s", message);

		fgets(line, 50, stdin);
//		fflush(stdin);
//		printf("%s\n", line);
		line[strlen(line)-1] = '\0';
		input = usrInput(line);

		//check input--str program or showjobs
//		int check = 0;
		char *var1 = input[0];
		if (strcmp(var1, "showjobs") == 0){
			//call for print jobs 
			queue_jobs(q, P);
		}
		else if(strcmp(var1, "submit") == 0) {
			//log input
			queue_insert(q, input[1]);
			printf("job %d added to the queue\n", q->count-1);			
			pid = fork();

			if (pid == 0){ // child
				//redirect code
//				printf("got to child\n");
//				queue_insert(q, &input[1]);
//				printf("job %d added to the queue\n", q->count-1);
				redirect();
				//printf("got to execvp\n");

				execvp(input[1], &input[1]);
				printf("If you see this, execvp failed\n");
				exit(-1);
			}

			else if (pid > 0){
//				printf("hehe\n");
				if (signal(SIGCHLD, sig_child) == SIG_ERR){
					printf("unable to catch sigchild\n");
				}
//				waitpid(-1, &status, WNOHANG);		
				sleep(1);
				//closeRedirect
//				close(fdout);
//				close(ferr);
			}

			else {
				perror("fork");
				exit(EXIT_FAILURE);
			}
		}
		//free(input);
		//fflush(stdout);
	}
	return 0;
}

static void sig_child(int signo){
	int status;
//	waitpid(pid, &status, WNOHANG | WUNTRACED);
	if (WIFEXITED(status)){queue_delete(q);}
	//queue_delete(q);
	//need to delete first in queue 
}
int redirect(){
	char childerror[30];
	char childout[30];
	int id = q->count;
	sprintf(childerror, "%d", id);
	strcpy(childout, childerror);
	strcat(childout, ".out");
	strcat(childerror, ".err");

	if ((fdout = open(childout, O_CREAT | O_APPEND | O_WRONLY, 0755)) == -1){
		printf("error opening new child std out\n");
		exit(-1);
	}
	if ((ferr = open(childerror, O_CREAT | O_APPEND | O_WRONLY, 0755)) == -1){
		printf("error opening new child std err\n");
		exit(-1);
	}

	dup2(fdout, 1);
	dup2(ferr, 2);

	return 0;
}

