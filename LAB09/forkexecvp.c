/*From HW/LAB07 fork exec */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

pid_t pid;

static void sig_child(int signo){
	int status;

	signal (signo, SIG_IGN); //ignores other SIGINT signals
	waitpid(pid, &status, WNOHANG); 
	if (WIFEXITED(status)){
		printf("child process exited with status = %d\n", WEXITSTATUS(status));
	}
	else {
		printf("child process did not exit normally\n");
	}
	fflush(stdout);
	signal(signo, sig_child); 
}

static void sig_int(int signo){
	switch(signo){
		case SIGINT:
			kill(pid, SIGINT);
			printf("\nkilled child process\n");
			break;
		case SIGTSTP:
			kill(pid, SIGTSTP);
			printf("\nstopped child process\n");
			break;
		case SIGQUIT:
			exit(0);
		default:
			printf("recevied signal %d\n", signo);
	}
}

int main (int argc, char **argv){

	if (argc < 2){
		printf("Usage: %s <command> [args]\n", argv[0]);
		exit(-1);
	}

	pid = fork();
	if (pid == 0){ //child
		execvp(argv[1], &argv[1]);
		printf("If you see this statement then execl failed\n");
		exit(EXIT_FAILURE);
	}
	else if (pid > 0){ //parent
		if (signal(SIGCHLD, sig_child) == SIG_ERR){
			printf("unable to catch SIGCHLD\n");
		}
		printf("Wait for the child process to terminate\n");
		if (signal(SIGINT, sig_int) == SIG_ERR){
			printf("unable to catch SIGINT\n");
		}
		if (signal(SIGTSTP, sig_int) == SIG_ERR){
			printf("unable to catch SIGTSTP\n");
		}
		if (signal(SIGQUIT, sig_int) == SIG_ERR){
			printf("unable to catch SIGSTP\n");
		}
		for ( ; ; )
			pause();
	}
	else {
		perror("fork");
		exit(EXIT_FAILURE);		
	}
	
	printf("[%ld]: Exiting program.......\n", (long)getpid());

	return 0;
}

