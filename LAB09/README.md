LAB 09: Signal Handling

This assignment required us to intercept the ^Z and ^C signal send on the command line 
that would normally terminate both the parent and child and instead only kill
or suspend the child process. This program also intercepts the ^\ signal to 
quit the parent process. 

Acknowledgements:
https://stackoverflow.com/questions/16294153/what-is-the-signal-function-sigint
code provided by the professor 

Contact Info:
Stephen Scales
sgs44328@uab.edu

