#include <stdio.h>
#include <stdlib.h>

//function declaration
void displayValues(int *arr, int size);
int binarySearch(int value, int *arr, int size);

int main(){
	int i, size, temp, value, index;
	printf("Please provide me with the number of values in the array: ");
	scanf("%d", &size);

	int *arr = malloc(size*sizeof(int));
	for (i=0; i<size; i++){
		printf("What is the number you would like to add to the array:");
		scanf("%d", (arr+i));
	}

	printf("Great, now what value would you like me to find: ");
	scanf("%d", &value);

	displayValues(arr, size);
	index = binarySearch(value, arr, size);

	if (index >= 0) {
		printf("The index of your value in the array is : %d \n", index);
	}
	else {
		printf("I'm sorry, but that number is not in the list \n");
	}
}

void displayValues(int *arr, int size){
	int i;
	for (i=0; i<size; i++){
		printf("%d\n", *(arr+i));
	}
}

int binarySearch(int value, int *arr, int size){
	int i, midpoint, curr, temp, low, high;
	for (i=0; i<size; i++){
		curr = i;
		while (curr > 0 && *(arr+curr-1) > *(arr+curr)){
			temp = *(arr+curr);
			*(arr+curr) = *(arr+curr-1);
			*(arr+curr-1) = temp;
			curr--;
		}
	}
	/* test : displayValues(arr, size);*/

	high = size-1;
	low = 0;
	while (low <= high){
		midpoint = (low+high)/2;
		if (*(arr+midpoint) == value){return midpoint;}
		else if (*(arr+midpoint) < value){
			low = midpoint + 1;
		}
		else if (*(arr+midpoint) > value){
			high = midpoint -1;
		}
	}
	return -1;
}

