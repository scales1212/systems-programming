#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>

#define BUFFSIZE 4096


/*Used pubs.opengroup.org as references for some functions, as well as the book and TA provided code for lseek, write, read */
int binarySearch(char arr[12000][50], int size, char value[50]);
void insertionSort(char arr[12000][50], int size);

int main(int argc, char **argv){
	int readFileDescriptor, writeFileDescriptor;
	char file[] = {"newfile.txt"};
	char buf[BUFFSIZE];
	char arr[12000][50];
	long int n;
	
	readFileDescriptor = open(argv[1], O_RDONLY);
	
	int i = 0;
	int k = 1;
	while(n=read(readFileDescriptor,buf, BUFFSIZE)> 0){
		//char temp[50];
		
	//	const char c[5] = "\n";
		char* tok = strtok(buf, "\n");
		//printf("%s\n", tok);
		while (tok != NULL){
			//strcat(tok, "\n");
			strcpy(arr[i], tok);
			tok = strtok(NULL, "\n");
			i++;
			//printf("%s\n", tok);
			k++;		
		}
	}

	printf("%d\n", k);
	insertionSort(arr, k);
// 	for (i=0; i<k; i++){
//		printf("%s\n", arr[i]);
//	}
	
	/* Writes to newfile.txt */
	writeFileDescriptor = open(file, O_CREAT|O_WRONLY|O_TRUNC, 0700);
	if (writeFileDescriptor == -1){
		printf("Error with file open \n");
		exit(-1);
	}
			
	for (i=0; i<k; i++){
		char str[50];
		strcpy(str, arr[i]);
		strcat(str, "\n");
		int ln;
		ln = strlen(str);
		write(writeFileDescriptor, str, ln);	
	}
	char value[50];
	int index;
	//gets user name to change
	printf("Now, what name would you like me to find and change: \n");
	scanf("%s", value);

	index = binarySearch(arr, k, value);
	if (index < 0){
		printf("Im sorry, but we could not locate that name in the list\n");
		return -1;
	}
	int totBytes = 0;
	for (i=0; i<index; i++){
		int len = strlen(arr[i]);
		totBytes += len;
	}	
	char replace[50];
	printf("What name would you like me to replace it with: ");
	scanf("%s", replace);

	/* lseeks to find entry to replace */
	if (lseek(writeFileDescriptor, totBytes, SEEK_SET) >= 0){
		write(writeFileDescriptor, replace, strlen(arr[index]));
	}

	close(readFileDescriptor);
	close(writeFileDescriptor);
}

void insertionSort(char arr[12000][50], int size){
	int i, j;

	for(i=0; i<size; i++){
		j = i; 
		//printf("%s\n", arr[i]);
		while (j > 0 && strcmp(arr[j-1], arr[j]) > 0){
			//printf("%s\n", arr[j]);
			char temp[50];
			strcpy(temp, arr[j]);
			strcpy(arr[j], arr[j-1]);
			strcpy(arr[j-1], temp);
			j--; 
		}
	}	
}

int binarySearch(char arr[12000][50], int size, char value[50]){
	int i, midpoint, low, high;

	high = size-1;
	low = 0;
	while (low <= high){
		midpoint = (low+high)/2;
		if (strcmp(arr[midpoint], value) == 0){
			return midpoint;
		}
		else if(strcmp(arr[midpoint], value) > 0){
			high = midpoint-1;
			//printf("%s", arr[midpoint]);
		}
		else if(strcmp(arr[midpoint], value) < 0){
			low = midpoint+1;
			//printf("%s", arr[midpoint]);
		}
	}
	return -1;
}


