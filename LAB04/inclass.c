#include <stdio.h>
#include <string.h>

int main(int argc, char** argv) {
	char hello[20] = {'h', 'e', 'l', 'l', 'o', ' ', '\0'};
	char name[] = "students!\0";
	printf("%s\n", name);
	int len = strlen(name);
	printf("%d", len);
	printf("%c\n", name);
	strcat(hello, name);
	printf("%s\n", hello);
	
	return 0;
}
